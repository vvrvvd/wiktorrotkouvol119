﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour {

    [HideInInspector]
    public Rigidbody2D Rigidbody2D;
    [HideInInspector]
    public CircleCollider2D CircleCollider;
    [HideInInspector]
    public PointEffector2D PointEffector;

    public int BallsCounterInside;
    public int MaxMergedBalls = 5;

    private Spawner spawner;
    private float startMass;

    private bool isCollisionActive;
    private Vector3 startLocalScale;

    private void Awake()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
        CircleCollider = GetComponent<CircleCollider2D>();
        PointEffector = GetComponentInChildren<PointEffector2D>();
        startMass = Rigidbody2D.mass;
        startLocalScale = transform.localScale;
    }

    private IEnumerator Start()
    {
        Assert.IsNotNull(PointEffector);

        while (Spawner.Instance == null)
            yield return new WaitForEndOfFrame();

        spawner = Spawner.Instance;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isCollisionActive && collision.collider.CompareTag("Ball"))
        {
            Ball otherBall = collision.collider.GetComponent<Ball>();
            MergeWith(otherBall);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(isCollisionActive && collision.collider.CompareTag("Ball"))
        {
            Ball otherBall = collision.collider.GetComponent<Ball>();
            MergeWith(otherBall);
        }
    }

    public void MergeWith(Ball otherBall)
    {
        if (otherBall.isCollisionActive)
        {
            otherBall.TurnOffCollisions();
            otherBall.spawner.ReturnToPool(otherBall.gameObject);
            BallsCounterInside += otherBall.BallsCounterInside;
            transform.localScale = BallsCounterInside * startLocalScale;
            Rigidbody2D.mass = BallsCounterInside * startMass;

            CheckExplosion();
        }
    }

    private void CheckExplosion()
    {
        if (BallsCounterInside >= MaxMergedBalls)
        {
            TurnOffCollisions();
            spawner.ReturnToPool(gameObject);
            spawner.ExplodeBalls(transform.position, BallsCounterInside);
        }
    }

    public void TurnOffCollisions()
    {
        isCollisionActive = false;
        PointEffector.enabled = false;
    }

    public void TurnOnCollisions()
    {
        isCollisionActive = true;
        PointEffector.enabled = true;
    }

    public void Resetball()
    {
        BallsCounterInside = 1;
        transform.localScale = startLocalScale;
        Rigidbody2D.mass = startMass;
        Rigidbody2D.velocity = Vector2.zero;
    }


}
