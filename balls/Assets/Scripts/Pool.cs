﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour{

    public GameObject ObjectPrefab;

    private int ballsCount;
    private Stack<GameObject> pool;
    private List<GameObject> allInstances;

    private void Awake()
    {
        pool = new Stack<GameObject>();
    }

    private void Start()
    {
        allInstances = new List<GameObject>();
    }

    public GameObject GetFromPool()
    {
        if(pool.Count > 0)
        {
            GameObject poolObject = pool.Pop();
            poolObject.SetActive(true);
            return poolObject;
        } else
        {
            ballsCount++;
            GameObject poolObject = Instantiate(ObjectPrefab);
            allInstances.Add(poolObject);
            return poolObject;
        }
    }

    public void ReturnToPool(GameObject obj)
    {
        pool.Push(obj);
        obj.SetActive(false);
    }

    public int GetBallsCount()
    {
        return ballsCount;
    }

    public List<GameObject> GetAllInstances()
    {
        return allInstances;
    }

}
