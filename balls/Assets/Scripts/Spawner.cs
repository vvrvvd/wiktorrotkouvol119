﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

    public static Spawner Instance;

    [SerializeField]
    private GameObject objectPrefab;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private Text counterText;

    [Header("Spawn")]
    [SerializeField]
    private float timeStamp;

    [SerializeField]
    private int maxBalls;

    [Header("Explosion")]
    [SerializeField]
    private int maxMergedBalls;

    [SerializeField]
    private int explosionForce;


    private int ballsCount;
    private bool isSpawning;

    private Pool ballPool;
    private Rect cameraViewRect;
    private IEnumerator spawnCoroutine;

    void Awake () {

        if (Instance!=null)
            Destroy(Instance);
        Instance = this;

        ballPool = gameObject.AddComponent<Pool>();
        ballPool.ObjectPrefab = objectPrefab;
    }

    private void Start()
    {
        Assert.IsNotNull(objectPrefab);
        Assert.IsNotNull(counterText);

        CalculateScreenBounds();
        isSpawning = true;
        spawnCoroutine = SpawnCoroutine();
        StartCoroutine(spawnCoroutine);
    }

    private void CalculateScreenBounds()
    {
        var height = mainCamera.orthographicSize;
        var width = height * Screen.width / Screen.height;

        cameraViewRect = new Rect(mainCamera.transform.position.x, mainCamera.transform.position.y, width, height);
    }

    private IEnumerator SpawnCoroutine()
    {
        while (isSpawning)
        {
            yield return new WaitForSeconds(timeStamp);
            var ballObject = ballPool.GetFromPool();
            Ball ball = ballObject.GetComponent<Ball>();
            ball.Resetball();
            ball.transform.position = GetPositionInsideCameraView();
            ball.TurnOnCollisions();
            ball.MaxMergedBalls = maxMergedBalls;
            ballsCount++;
            UpdateText();
            CheckStopConditions();
        }
    }

    private Vector3 GetPositionInsideCameraView()
    {
        float x = Random.Range(cameraViewRect.xMin, cameraViewRect.xMax);
        float y = Random.Range(cameraViewRect.yMin, cameraViewRect.yMax);
        return new Vector2(x, y);
    }

    private void CheckStopConditions()
    {
        if(ballsCount >= maxBalls)
        {
            isSpawning = false;
            OnMaxBallSpawned();
        }
    }

    private void OnMaxBallSpawned()
    {
        var allBalls = ballPool.GetAllInstances();
        for (int i = 0; i < allBalls.Count; i++)
        {
            Ball ball = allBalls[i].GetComponent<Ball>();
            ball.PointEffector.forceMagnitude *= -1;
        }
    }


    public void ReturnToPool(GameObject poolObject)
    {
        ballPool.ReturnToPool(poolObject);
    }

    private void UpdateText()
    {
        counterText.text = "Balls: " + ballsCount;
    }

    public void ExplodeBalls(Vector3 centerPosition, int counter)
    {
        List<Ball> ballsList = new List<Ball>();

        for(int i=0; i<counter; i++)
        {
            var ballObject = ballPool.GetFromPool();
            Ball ball = ballObject.GetComponent<Ball>();
            ball.Resetball();
            ball.TurnOffCollisions();
            ball.MaxMergedBalls = maxMergedBalls;
            ball.transform.position = centerPosition;
            ball.Rigidbody2D.velocity = new Vector2(Random.Range(-100, 100), Random.Range(-100, 100)).normalized * explosionForce;
            ballsList.Add(ball);
        }

        StartCoroutine(DelayedTurnOnCollision(ballsList, 0.5f));
    }

    private IEnumerator DelayedTurnOnCollision(List<Ball> balls, float delay)
    {
        yield return new WaitForSeconds(delay);

        for (int i = 0; i < balls.Count; i++)
        {
            Ball ball = balls[i].GetComponent<Ball>();
            ball.TurnOnCollisions();
        }
    }


}
