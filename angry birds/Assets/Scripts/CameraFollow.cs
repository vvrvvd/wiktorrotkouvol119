﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    [Range(0, 1)]
    private float smoothFactor;

    [SerializeField]
    private Transform targetTransform;

    private float startPositionZ;

    private void Start()
    {
        startPositionZ = transform.position.z;
    }

    private void LateUpdate () {
        CameraMovement();
	}

    private void CameraMovement()
    {
        if (targetTransform != null)
        {
            Vector3 nextPosition = Vector3.Lerp(transform.position, targetTransform.position, smoothFactor);
            nextPosition.z = startPositionZ;
            transform.position = nextPosition;
        }
    }

    public void SetFollowSmooth(float smoothFactor)
    {
        this.smoothFactor = Mathf.Clamp01(smoothFactor);
    }

    public void SetTargetObject(Transform target)
    {
        targetTransform = target;
    }
}
