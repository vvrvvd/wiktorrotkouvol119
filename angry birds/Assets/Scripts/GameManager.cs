﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour {

    public CameraFollow mainCameraFollow;
    public Slingshot slingshot;

    private IEnumerator delayedCameraChangeCoroutine;

	private void Start () {
        Assert.IsNotNull(slingshot);
        Assert.IsNotNull(mainCameraFollow);

        slingshot.OnBulletShoot.AddListener(ChangeCameraFollowToBullet);
	}
	
	private void ChangeCameraFollowToBullet()
    {
        Bullet bullet = slingshot.GetCurrentBullet();

        if(bullet!=null)
        {
            mainCameraFollow.SetTargetObject(bullet.transform);
            bullet.OnHitTheGround.AddListener(ChangeCameraFollowToSlingshot);
        }
    }

    private void ChangeCameraFollowToSlingshot()
    {
        if (delayedCameraChangeCoroutine != null)
            StopCoroutine(delayedCameraChangeCoroutine);

        delayedCameraChangeCoroutine = DelayedChangeCameraFollow(slingshot.transform, 1.0f);
        StartCoroutine(delayedCameraChangeCoroutine);
    }

    private IEnumerator DelayedChangeCameraFollow(Transform target, float delay)
    {
        yield return new WaitForSeconds(delay);
        mainCameraFollow.SetTargetObject(target);
    }
}
