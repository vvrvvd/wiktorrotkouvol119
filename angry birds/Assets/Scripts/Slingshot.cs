﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Slingshot : MonoBehaviour {

    public UnityEvent OnBulletShoot;

    [Header("Shooting")]
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private float maxForce;
    [SerializeField]
    private float maxClingDistance;


    private bool canShoot;
    private bool isShooting;
    private Camera mainCamera;
    private Bullet currentBullet;
    private Vector3 currentMouseRelativePosition;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update() {
        CheckShoot();
    }

    private void CheckShoot()
    {
        if(isShooting)
        {
            CheckInput();
        }
    }

    private void CheckInput()
    {
        if (Input.GetMouseButtonUp(0))
            ShootCurrentBullet();
        else if (Input.GetMouseButton(0))
            UpdateCurrentBulletPosition();
    }

    private void UpdateCurrentBulletPosition()
    {
        UpdateNextCurrentBulletPosition();
        currentBullet.transform.position = currentMouseRelativePosition;
    }

    private void UpdateNextCurrentBulletPosition()
    {
        currentMouseRelativePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        currentMouseRelativePosition.z = transform.position.z;
        ClampMousePosition();
    }

    private void ClampMousePosition()
    {
        float distance = Vector3.Distance(transform.position, currentMouseRelativePosition);
        if (distance >= maxClingDistance)
        {
            Vector3 dir = currentMouseRelativePosition - transform.position;
            currentMouseRelativePosition = transform.position + dir.normalized * maxClingDistance;
        }
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 bulletStartPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            bulletStartPosition.z = transform.position.z;
            InstantiateBullet(bulletStartPosition);
            isShooting = true;
        }
    }

    private void InstantiateBullet(Vector3 position)
    {
        currentBullet = Instantiate(bulletPrefab).GetComponent<Bullet>();
        currentBullet.OnHitTheGround.AddListener(Reload);
        currentBullet.transform.position = position;
    }

    private void ShootCurrentBullet()
    {
        Vector2 dir = transform.position - currentBullet.transform.position;
        currentBullet.AddForce(dir, maxForce);
        canShoot = false;
        isShooting = false;
        OnBulletShoot.Invoke();
    }

    public void Reload()
    {
        canShoot = true;
    }

    public Bullet GetCurrentBullet()
    {
        return currentBullet;
    }

}
