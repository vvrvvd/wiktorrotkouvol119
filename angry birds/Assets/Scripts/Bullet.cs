﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour {

    public UnityEvent OnHitTheGround;

    private Rigidbody2D rgdb2d;
    private bool firstHit;

    private void Awake()
    {
        rgdb2d = GetComponent<Rigidbody2D>();
    }

    public void AddForce(Vector2 direction, float force)
    {
        rgdb2d.AddForce(direction * force);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(!firstHit && collision.collider.CompareTag("Ground"))
        {
            OnHitTheGround.Invoke();
            firstHit = true;
        }
    }
}
